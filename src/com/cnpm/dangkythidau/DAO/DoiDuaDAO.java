package com.cnpm.dangkythidau.DAO;

import com.cnpm.dangkythidau.Model.DoiDua;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DoiDuaDAO extends DAO{

    public List<DoiDua> getAllDoiDua(){
        List<DoiDua> result = new ArrayList<>();
        String sql = "SELECT * FROM tblDoiDua";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()){
                int id = resultSet.getInt(1);
                String tenDoi = resultSet.getString("tenDoiDua");
                DoiDua doiDua = new DoiDua(id, tenDoi);
                result.add(doiDua);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
