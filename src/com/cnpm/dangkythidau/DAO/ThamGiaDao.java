package com.cnpm.dangkythidau.DAO;

import com.cnpm.dangkythidau.Model.ChangDua;
import com.cnpm.dangkythidau.Model.TayDuaDangKi;
import com.cnpm.dangkythidau.Model.TayDuaThamGia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ThamGiaDao extends DAO{

        public boolean saveThamGia(TayDuaThamGia thamGia){
            String sql = "INSERT INTO tblTayDuaThamGia(tenChangDua,tenDoiDua,tenTayDua,idDangKi,tuoi,quocTich) values (?,?,?,?,?,?) ";
            try{  PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, thamGia.getTenChangDua());
                ps.setString(2,thamGia.getTenDoiDua());
                ps.setString(3, thamGia.getTenTayDua());
                ps.setInt(4, thamGia.getIdDangKi());
                ps.setInt(5,thamGia.getTuoi());
                ps.setString(6,thamGia.getQuocTich());
                ps.executeUpdate();
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()){
                    thamGia.setIdThamGia(generatedKeys.getInt(1));}
            }catch (Exception e){
                e.printStackTrace();
            }
            return  true;
        }
    }