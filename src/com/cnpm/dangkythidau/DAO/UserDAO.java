package com.cnpm.dangkythidau.DAO;

import com.cnpm.dangkythidau.Model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO extends DAO{
    public UserDAO() {
        super();
            }


public boolean checkLogin(User user){
        boolean result = false;
        String sql = "SELECT ten, tuoi, chucVu FROM tblUser WHERE userName = ? AND passWord = ?";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getPassWord());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                user.setTen(rs.getString("ten"));
                user.setChucVu(rs.getString("chucVu"));
                result = true;
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
}
}
