package com.cnpm.dangkythidau.DAO;

import com.cnpm.dangkythidau.Model.ChangDua;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ChangDuaDAO extends DAO {
    public List<ChangDua> getAllChang(){
        List<ChangDua> res = new ArrayList<>();
        try{
            String sql = "SELECT * From tblChangDua";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()){
                int idChangDua = resultSet.getInt(1);
                String diaDiem = resultSet.getString(2);
                ChangDua c = new ChangDua(idChangDua, diaDiem);
                res.add(c);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return res;
    }

}
