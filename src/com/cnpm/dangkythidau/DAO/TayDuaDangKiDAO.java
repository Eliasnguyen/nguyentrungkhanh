package com.cnpm.dangkythidau.DAO;

import com.cnpm.dangkythidau.Model.TayDuaDangKi;
import com.cnpm.dangkythidau.Model.TayDuaThamGia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TayDuaDangKiDAO extends DAO {
    public boolean saveDangKi(TayDuaDangKi tayDuaDangKi) {
        String sql = "INSERT INTO tblTayDuaDangKi (tenTayDua,tuoi,quocTich,tenDoiDua,tenChangDua) values (?,?,?,?,?) ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, tayDuaDangKi.getTenTayDua());
            ps.setInt(2, tayDuaDangKi.getTuoi());
            ps.setString(3, tayDuaDangKi.getQuocTich());
            ps.setString(4, tayDuaDangKi.getTenDoiDua());
            ps.setString(5, tayDuaDangKi.getTenChangDua());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                tayDuaDangKi.setIdDangKi(generatedKeys.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    public List<TayDuaDangKi> findDangKi() {

        List<TayDuaDangKi> res = new ArrayList<>();
        String sql = "select DISTINCT a.tenChangDua\n" +
                "from\n" +
                " tblTayDuaDangKi as a , tblTayDuaThamGia as b \n" +
                "where a.idDangKi != b.idDangKi";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                TayDuaDangKi dk = new TayDuaDangKi();
//                dk.setIdDangKi(rs.getInt("idDangKi"));
//                dk.setTenTayDua(rs.getString("tenTayDua"));
//                dk.setTenDoiDua(rs.getString("tenDoiDua"));
                dk.setTenChangDua(rs.getString("tenChangDua"));
//                dk.setQuocTich(rs.getString("quocTich"));
//                dk.setTuoi(rs.getInt("tuoi"));
                res.add(dk);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public List<TayDuaDangKi> findDoiDangKi(String tenChang) {
        List<TayDuaDangKi> resu = new ArrayList<>();
        String sql = "select DISTINCT a.tenDoiDua,a.tenChangDua\n" +
                "from\n" +
                " tblTayDuaDangKi as a , tblTayDuaThamGia as b \n" +
                "where a.idDangKi != b.idDangKi AND a.tenChangDua like ?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + tenChang + "%");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                TayDuaDangKi dk = new TayDuaDangKi();
                dk.setTenDoiDua(rs.getString("tenDoiDua"));
                dk.setTenChangDua(rs.getString("tenChangDua"));
                resu.add(dk);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resu;
    }

    public List<TayDuaDangKi> findTayDuaDangKi(String tenChangDua, String tenDoiDua) {
        List<TayDuaDangKi> resu = new ArrayList<>();
        String sql = "select DISTINCT a.tenChangDua,a.tenDoiDua, a.tenTayDua, a.tuoi, a.quocTich,a.idDangKi \n" +
                "from tblTayDuaDangKi as a , tblTayDuaThamGia as b \n" +
                "where a.idDangKi != b.idDangKi AND a.tenDoiDua like ? AND a.tenChangDua like ?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + tenDoiDua + "%");
            ps.setString(2, "%" + tenChangDua + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TayDuaDangKi dk = new TayDuaDangKi();
                dk.setTenTayDua(rs.getString("tenTayDua"));
                dk.setQuocTich(rs.getString("quocTich"));
                dk.setTuoi(rs.getInt("tuoi"));
                dk.setTenChangDua(rs.getString("tenChangDua"));
                dk.setTenDoiDua(rs.getString("tenDoiDua"));
                dk.setIdDangKi(rs.getInt("idDangKi"));
                resu.add(dk);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resu;
    }

    public void deleteARecord(int idDangKi) {
        String sql = "DELETE from tblTayDuaDangKi WHERE idDangKi = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, idDangKi);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
