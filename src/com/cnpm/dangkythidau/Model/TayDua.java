package com.cnpm.dangkythidau.Model;

import java.io.Serializable;

public class TayDua implements Serializable {
    private int idTayDua;
    private String ten;
    private int age;
    private String quocTich;

    public TayDua() {
        super();
    }

    public TayDua(int idTayDua, String ten, int age, String quocTich) {
        this.idTayDua = idTayDua;
        this.ten = ten;
        this.age = age;
        this.quocTich = quocTich;
    }

    public int getIdTayDua() {
        return idTayDua;
    }

    public void setIdTayDua(int idTayDua) {
        this.idTayDua = idTayDua;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }
}
