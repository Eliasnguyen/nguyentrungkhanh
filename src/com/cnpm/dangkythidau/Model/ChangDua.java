package com.cnpm.dangkythidau.Model;

import java.io.Serializable;
import java.util.Date;

public class ChangDua implements Serializable {
    private int idChangDua;
    private String diaDiem;
    private Date thoiGian;
    private float quangDuong;
    private int soVong;
    private int soKhucCua;
    private String loaiDuongDua;

    public ChangDua() {
        super();
    }

    public ChangDua(int idChangDua, String diaDiem) {
        this.idChangDua = idChangDua;
        this.diaDiem = diaDiem;
    }

    public ChangDua(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    public int getIdChangDua() {
        return idChangDua;
    }

    public void setIdChangDua(int idChangDua) {
        this.idChangDua = idChangDua;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    public Date getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(Date thoiGian) {
        this.thoiGian = thoiGian;
    }

    public float getQuangDuong() {
        return quangDuong;
    }

    public void setQuangDuong(float quangDuong) {
        this.quangDuong = quangDuong;
    }

    public int getSoVong() {
        return soVong;
    }

    public void setSoVong(int soVong) {
        this.soVong = soVong;
    }

    public int getSoKhucCua() {
        return soKhucCua;
    }

    public void setSoKhucCua(int soKhucCua) {
        this.soKhucCua = soKhucCua;
    }

    public String getLoaiDuongDua() {
        return loaiDuongDua;
    }

    public void setLoaiDuongDua(String loaiDuongDua) {
        this.loaiDuongDua = loaiDuongDua;
    }
}
