package com.cnpm.dangkythidau.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class TayDuaDangKi implements Serializable {
    private int idDangKi;
    private String tenTayDua;
    private int tuoi;
    private String quocTich;
    private String tenChangDua;
    private String tenDoiDua;
    private ArrayList<TayDuaThamGia> tdThamGia;

    public TayDuaDangKi() {
    }

    public TayDuaDangKi(String tenChangDua, String tenDoiDua) {
        this.tenChangDua = tenChangDua;
        this.tenDoiDua = tenDoiDua;
    }

    public TayDuaDangKi(String tenTayDua,int tuoi, String quocTich, String tenChangDua, String tenDoiDua) {
        this.tenTayDua = tenTayDua;
        this.tuoi = tuoi;
        this.quocTich = quocTich;
        this.tenChangDua = tenChangDua;
        this.tenDoiDua = tenDoiDua;
    }

    public int getIdDangKi() {
        return idDangKi;
    }

    public void setIdDangKi(int idDangKi) {
        this.idDangKi = idDangKi;
    }

    public String getTenTayDua() {
        return tenTayDua;
    }

    public void setTenTayDua(String tenTayDua) {
        this.tenTayDua = tenTayDua;
    }

    public String getTenChangDua() {
        return tenChangDua;
    }

    public void setTenChangDua(String tenChangDua) {
        this.tenChangDua = tenChangDua;
    }

    public String getTenDoiDua() {
        return tenDoiDua;
    }

    public void setTenDoiDua(String tenDoiDua) {
        this.tenDoiDua = tenDoiDua;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }
}
