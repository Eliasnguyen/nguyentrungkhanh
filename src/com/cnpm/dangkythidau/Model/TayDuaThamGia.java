package com.cnpm.dangkythidau.Model;

import java.io.Serializable;

//lam gion booked

public class TayDuaThamGia implements Serializable {
    private int idThamGia;
    private String tenChangDua;
    private String tenDoiDua;
    private String tenTayDua;
    private int tuoi;
    private String quocTich;
    private int idDangKi;
    private int idChangDua;
    private int idDoiDua;
    private int idTayDua;

    private ChangDua changDua;
    private TayDua tayDua;
    private DoiDua doiDua;
    private boolean isChecked;

    public TayDuaThamGia() {
        super();
    }

    public TayDuaThamGia(String tenChangDua, String tenDoiDua,String  tenTayDua, int tuoi, String quocTich, int idDangki) {
        this.tenChangDua = tenChangDua;
        this.tenDoiDua = tenDoiDua;
        this.tenTayDua = tenTayDua;
        this.tuoi = tuoi;
        this.quocTich = quocTich;
        this.idDangKi = idDangki;
    }


    public String getTenChangDua() {
        return tenChangDua;
    }

    public void setTenChangDua(String tenChangDua) {
        this.tenChangDua = tenChangDua;
    }

    public String getTenDoiDua() {
        return tenDoiDua;
    }

    public void setTenDoiDua(String tenDoiDua) {
        this.tenDoiDua = tenDoiDua;
    }

    public String getTenTayDua() {
        return tenTayDua;
    }

    public void setTenTayDua(String tenTayDua) {
        this.tenTayDua = tenTayDua;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    public int getIdDangKi() {
        return idDangKi;
    }

    public void setIdDangKi(int idDangKi) {
        this.idDangKi = idDangKi;
    }

    public int getIdChangDua() {
        return idChangDua;
    }

    public void setIdChangDua(int idChangDua) {
        this.idChangDua = idChangDua;
    }

    public int getIdDoiDua() {
        return idDoiDua;
    }

    public void setIdDoiDua(int idDoiDua) {
        this.idDoiDua = idDoiDua;
    }

    public int getIdTayDua() {
        return idTayDua;
    }

    public void setIdTayDua(int idTayDua) {
        this.idTayDua = idTayDua;
    }

    public int getIdThamGia() {
        return idThamGia;
    }

    public void setIdThamGia(int idThamGia) {
        this.idThamGia = idThamGia;
    }

    public ChangDua getChangDua() {
        return changDua;
    }

    public void setChangDua(ChangDua changDua) {
        this.changDua = changDua;
    }

    public TayDua getTayDua() {
        return tayDua;
    }

    public void setTayDua(TayDua tayDua) {
        this.tayDua = tayDua;
    }

    public DoiDua getDoiDua() {
        return doiDua;
    }

    public void setDoiDua(DoiDua doiDua) {
        this.doiDua = doiDua;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
