package com.cnpm.dangkythidau.Model;

import java.io.Serializable;

public class User implements Serializable {
    private int idBtc;
    private String userName;
    private String passWord;
    private String ten;
    private int tuoi;
    private String chucVu;


    public User() {
    }

    public User(int idBtc, String userName, String passWord, String ten, int tuoi, String chucVu) {
        this.idBtc = idBtc;
        this.userName = userName;
        this.passWord = passWord;
        this.ten = ten;
        this.tuoi = tuoi;
        this.chucVu = chucVu;
    }

    public int getIdBtc() {
        return idBtc;
    }

    public void setIdBtc(int idBtc) {
        this.idBtc = idBtc;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }
}
