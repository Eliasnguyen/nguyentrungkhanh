package com.cnpm.dangkythidau.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class DoiDua implements Serializable {
    private int idDoiDua;
    private String ten;
    private float tongThoiGian;
    private float tongDiem;
    private ArrayList<TayDua> tayDuaArrayList;

    public DoiDua(){
        super();
    }

    public DoiDua(int idDoiDua, String ten) {
        this.idDoiDua = idDoiDua;
        this.ten = ten;
    }

    public DoiDua(int idDoiDua, String ten, float tongThoiGian, float tongDiem) {
        this.idDoiDua = idDoiDua;
        this.ten = ten;
        this.tongThoiGian = tongThoiGian;
        this.tongDiem = tongDiem;
    }

    public int getIdDoiDua() {
        return idDoiDua;
    }

    public void setIdDoiDua(int idDoiDua) {
        this.idDoiDua = idDoiDua;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public float getTongThoiGian() {
        return tongThoiGian;
    }

    public void setTongThoiGian(float tongThoiGian) {
        this.tongThoiGian = tongThoiGian;
    }

    public float getTongDiem() {
        return tongDiem;
    }

    public void setTongDiem(float tongDiem) {
        this.tongDiem = tongDiem;
    }
}
