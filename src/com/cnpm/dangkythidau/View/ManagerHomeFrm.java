package com.cnpm.dangkythidau.View;

import com.cnpm.dangkythidau.Model.User;
import com.cnpm.dangkythidau.Model.TayDuaDangKi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ManagerHomeFrm extends JFrame implements ActionListener {
    private JButton btnDangki;
    private User user;
    private TayDuaDangKi dangKi;
    public ManagerHomeFrm(User user) {
        super("Manager home");
        this.user = user;

        JPanel listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));

        JPanel lblPane = new JPanel();
        lblPane.setLayout(new BoxLayout(lblPane, BoxLayout.LINE_AXIS));
        lblPane.add(Box.createRigidArea(new Dimension(450, 0)));
        JLabel lblUser = new JLabel("Loged in as: " + user.getTen());
        lblUser.setAlignmentX(Component.RIGHT_ALIGNMENT);
        lblPane.add(lblUser);
        listPane.add(lblPane);
        listPane.add(Box.createRigidArea(new Dimension(0,20)));

        JLabel lblHome = new JLabel("Manager's home");
        lblHome.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblHome.setFont (lblHome.getFont ().deriveFont (28.0f));
        listPane.add(lblHome);
        listPane.add(Box.createRigidArea(new Dimension(0,20)));

        btnDangki = new JButton("Dang ki tay dua");
        btnDangki.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnDangki.addActionListener(this);
        listPane.add(btnDangki);
        listPane.add(Box.createRigidArea(new Dimension(0,10)));


        this.setSize(600,300);
        this.setLocation(200,10);
        this.add(listPane, BorderLayout.CENTER);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        if((e.getSource() instanceof JButton)&&(((JButton)e.getSource()).equals(btnDangki))) {
            (new XacNhanFrm()).setVisible(true);
            this.dispose();
        }else {
            JOptionPane.showMessageDialog(this, "This function is under construction!");
        }
    }

}
