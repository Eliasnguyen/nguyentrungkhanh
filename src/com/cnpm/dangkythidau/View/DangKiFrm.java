package com.cnpm.dangkythidau.View;

import java.util.List;

import com.cnpm.dangkythidau.DAO.ChangDuaDAO;
import com.cnpm.dangkythidau.DAO.DoiDuaDAO;
import com.cnpm.dangkythidau.DAO.TayDuaDangKiDAO;
import com.cnpm.dangkythidau.Model.User;
import com.cnpm.dangkythidau.Model.ChangDua;
import com.cnpm.dangkythidau.Model.DoiDua;
import com.cnpm.dangkythidau.Model.TayDuaDangKi;
import com.mysql.jdbc.log.Log;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DangKiFrm extends JFrame implements ActionListener {
    private TayDuaDangKi dangKi;
    private JTextField txtTenTayDua;
    private JButton btnDangKi, btnInsert;
    private JList b;
    private final int WIDTH = 500;
    private final int HEIGHT = 500;
    private JComboBox changBox, doiDuaBox;
    private List<ChangDua> listChang;
    private List<DoiDua> listDoi;
    private DefaultComboBoxModel changModel, doiModel;
    private JTextField txtTuoiTayDua, txtQuocTich;
    private User user;

    public DangKiFrm(User user, TayDuaDangKi dangKi) {

        super("Dang ki thi dau ");
        this.user = user;
        this.dangKi = dangKi;
        this.setSize(WIDTH + 200, HEIGHT + 200);
        this.setLayout(null);
        this.setLocation(200, 10);

        btnInsert = new JButton("Dang Ki");
        btnInsert.setBounds(100, 400, 300, 40);
        btnInsert.addActionListener(this);
        this.add(btnInsert);


        //hien thi button nhap thong tin

        //add labels chon chang dua chon tay dua
        JPanel pnChang = new JPanel();
        pnChang.add(new JLabel("Chon chang dua:"));
        pnChang.setLayout(new BoxLayout(pnChang, BoxLayout.X_AXIS));
        pnChang.setBounds(10, 10, 200, 20);
        this.add(pnChang);

        JPanel pnDoi = new JPanel();
        pnDoi.add(new JLabel("Chon doi dua:"));
        pnDoi.setLayout(new BoxLayout(pnDoi, BoxLayout.X_AXIS));
        pnDoi.setBounds(10, 90, 200, 20);
        this.add(pnDoi);

        initWidgetsChangDua();
        initWidgetsDoiDua();
        initWidgetsInsert();

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);


        loadChang();
        loadDoiDua();


    }

    void initWidgetsChangDua() {

        changModel = new DefaultComboBoxModel<String>();
        changBox = new JComboBox<>();
        changBox.setModel(changModel);
        changBox.addActionListener(this);
        changBox.setBounds(10, 40, 200, 50);
        this.add(changBox);
    }

    void initWidgetsDoiDua() {
        doiModel = new DefaultComboBoxModel<String>();
        doiDuaBox = new JComboBox<>();
        doiDuaBox.setModel(doiModel);
        doiDuaBox.addActionListener(this);
        doiDuaBox.setBounds(10, 120, 200, 50);
        this.add(doiDuaBox);

    }


    private void loadChang() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ChangDuaDAO cdDao = new ChangDuaDAO();
                listChang = cdDao.getAllChang();
                changModel.removeAllElements();
                changModel.addElement("");
                for (ChangDua i : listChang) {
                    changModel.addElement(i.getDiaDiem());
                }
            }
        }).start();
    }

    private void loadDoiDua() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DoiDuaDAO doiDuaDAO = new DoiDuaDAO();
                listDoi = doiDuaDAO.getAllDoiDua();
                doiModel.addElement("");
                for (DoiDua i : listDoi) {
                    doiModel.addElement(i.getTen());
                }
            }
        }).start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
//        String tenChang;
//        String tenDoi, tenTayDua, quocTich;
//        int tuoi;
//        int index1, index2;
//          if (e.getSource() instanceof JComboBox && e.getSource().equals(changBox)) {
//              JComboBox box = (JComboBox) e.getSource();
//              index1 = box.getSelectedIndex();
             // tenChang = box.getSelectedItem().toString();
              //System.out.println(tenChang);
//          }
        if (e.getSource() instanceof JComboBox && e.getSource().equals(doiDuaBox)) {
//            JComboBox box2 = (JComboBox) e.getSource();
//            index2 = box2.getSelectedIndex();
//            tenDoi = box2.getSelectedItem().toString();
//            System.out.println(tenDoi);
//            if (index2 - 1 >= 0 && index2 - 1 < listDoi.size()) {
//                initWidgetsInsert();
//            }
        }

            if ((e.getSource() instanceof JButton) && (((JButton) e.getSource()).equals(btnInsert))) {
                String tenChang;
                String tenDoi, tenTayDua, quocTich;
                int tuoi;
                int index1, index2;
                index1 = changBox.getSelectedIndex();
                tenChang = listChang.get(index1-1).getDiaDiem();

                index2 = doiDuaBox.getSelectedIndex();
                tenDoi = listDoi.get(index2-1).getTen();

                tenTayDua = txtTenTayDua.getText();
                quocTich = txtQuocTich.getText();

                String tuoiTx = txtTuoiTayDua.getText();
                tuoi = Integer.parseInt(tuoiTx);

                TayDuaDangKi td = new TayDuaDangKi(tenTayDua, tuoi, quocTich, tenChang, tenDoi);
                TayDuaDangKiDAO dangKiDAO = new TayDuaDangKiDAO();
                if(dangKiDAO.saveDangKi(td)){
                    JOptionPane.showMessageDialog(this, "Gui thong tin dang ki thanh cong!");
                   (new LoginFrm()).setVisible(true);
                    this.dispose();
                }
            }
        }
        // neu bat buoc phai chon 2 cai tren roi moi chon cai duo

    void initWidgetsInsert() {

        txtTenTayDua = new JTextField();
        txtTuoiTayDua = new JTextField();
        txtQuocTich = new JTextField();

        JLabel lbTenTayDua = new JLabel("Nhap ten tay dua:");
        lbTenTayDua.setBounds(10, 180, 150, 20);
        this.add(lbTenTayDua);
        txtTenTayDua.setBounds(160, 180, 200, 20);
        this.add(txtTenTayDua);

        //nhap tuoi

        JLabel lbTuoi = new JLabel("Nhap tuoi tay dua:");
        lbTuoi.setBounds(10, 210, 150, 20);
        this.add(lbTuoi);
        txtTuoiTayDua.setBounds(160, 210, 200, 20);
       // txtTuoiTayDua.setText("tuoi");
        this.add(txtTuoiTayDua);

        //nhap quoctich
        JLabel lbQuocTich = new JLabel("Quoc tich tay dua:");
        lbQuocTich.setBounds(10, 240, 150, 20);
        this.add(lbQuocTich);
        txtQuocTich.setBounds(160, 240, 200, 20);
        this.add(txtQuocTich);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

}

