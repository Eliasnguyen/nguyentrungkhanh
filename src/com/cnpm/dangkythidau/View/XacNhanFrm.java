package com.cnpm.dangkythidau.View;

import com.cnpm.dangkythidau.DAO.TayDuaDangKiDAO;
import com.cnpm.dangkythidau.Model.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import static javax.swing.UIManager.get;

public class XacNhanFrm extends JFrame implements ActionListener {
    private JButton btnXacNhan2;
    private final int WIDTH = 500;
    private final int HEIGHT = 900;
    private JComboBox changBox;
    private List<TayDuaDangKi> listDangKi, listTim;
    private DefaultComboBoxModel changModel;
    private JTable  doiDuaTable;
    private DefaultTableModel dataModel;
    private String[] columns = {"Chang Dua", "Doi dua"};
    private JScrollPane pane;

    public XacNhanFrm() {
        super("Xac nhan dang ki thi dau ");
        this.setSize(WIDTH + 200, HEIGHT + 200);
        this.setLayout(null);
        this.setLocation(200, 10);

        btnXacNhan2 = new JButton("Xac nhan dang ki");
        btnXacNhan2.setBounds(250, 60, 250, 40);
        btnXacNhan2.addActionListener(this);
        this.add(btnXacNhan2);
        //hien thi button nhap thong tin

        //add labels chon chang dua chon tay du

        JPanel pnDoi = new JPanel();
        pnDoi.add(new JLabel("Chon Chang dua:"));
        pnDoi.setLayout(new BoxLayout(pnDoi, BoxLayout.X_AXIS));
        pnDoi.setBounds(10, 20, 200, 20);
        this.add(pnDoi);

        initWidgetsChangDua();
        loadChang();

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);


        dataModel = new DefaultTableModel(null, columns);
        doiDuaTable = new JTable();
        doiDuaTable.setModel(dataModel);
        doiDuaTable.setBounds((WIDTH - 500) / 2, 200, 500, 500);

        pane = new JScrollPane();
        pane.setViewportView(doiDuaTable);
        pane.setBounds((WIDTH - 500) / 2, 200, 500, 500);
        doiDuaTable.getTableHeader().setBackground(Color.WHITE);

        doiDuaTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int column = doiDuaTable.getColumnModel().getColumnIndexAtX(e.getX());
                int row = e.getY() / doiDuaTable.getRowHeight();
                if (row < doiDuaTable.getRowCount() && row >= 0 && column < doiDuaTable.getColumnCount() && column >= 0) {
                    System.out.println(listTim.get(row).toString());
                    new UpdateThamGiaFrm(listTim.get(row));

                }
            }
        });
        this.add(pane);

    }


    void initWidgetsChangDua() {
        changModel = new DefaultComboBoxModel<String>();
        changBox = new JComboBox<>();
        changBox.setModel(changModel);
        changBox.addActionListener(this);
        changBox.setBounds(10, 60, 200, 50);
        this.add(changBox);

    }

    private void loadChang() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                TayDuaDangKiDAO dkDao = new TayDuaDangKiDAO();
                listDangKi = dkDao.findDangKi();
                changModel.removeAllElements();
                changModel.addElement("");
                for (TayDuaDangKi i : listDangKi) {
                    changModel.addElement(i.getTenChangDua());
                }
            }
        }).start();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JComboBox && e.getSource().equals(changBox)) {
            JComboBox box = (JComboBox) e.getSource();
            String tenChang = box.getSelectedItem().toString();
            String key = tenChang.toLowerCase().trim();
            System.out.println(tenChang);

            TayDuaDangKiDAO dkDAO = new TayDuaDangKiDAO();
            listTim = dkDAO.findDoiDangKi(key);

            System.out.println("SIze: " + listTim.size());
            final String[][] data = new String[listTim.size()][2];
            for (int i = 0; i < listTim.size(); i++) {
                System.out.println(listTim.get(i));
                //"Ten Tay Dua", "Tuoi", "Quoc tich", "Chang Dua", "Doi dua"
                final TayDuaDangKi dk = listTim.get(i);
                data[i][0] = "" + dk.getTenChangDua();
                data[i][1] = "" + dk.getTenDoiDua();
            }
            dataModel.setDataVector(data, columns);
            dataModel.fireTableDataChanged();

        }
//        doiDuaTable.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                int column = doiDuaTable.getColumnModel().getColumnIndexAtX(e.getX());
//                int row = e.getY() / doiDuaTable.getRowHeight();
//                if (row < doiDuaTable.getRowCount() && row >= 0 && column < doiDuaTable.getColumnCount() && column >= 0) {
//                    System.out.println(lisTim.get(row);
//
//                }
//            }
//        });
//        this.add(pane);
//        if ((e.getSource() instanceof JButton) && (((JButton) e.getSource()).equals(btnXacNhan))) {
//            TayDuaDangKiDAO dangKiDAO = new TayDuaDangKiDAO();
//            if (dangKiDAO.saveDangKi(thamGia)) {
//                JOptionPane.showMessageDialog(this, "Dang ki thanh cong!");
//            }
    }
}
