package com.cnpm.dangkythidau.View;

import com.cnpm.dangkythidau.DAO.TayDuaDangKiDAO;
import com.cnpm.dangkythidau.DAO.ThamGiaDao;
import com.cnpm.dangkythidau.Model.TayDuaDangKi;
import com.cnpm.dangkythidau.Model.TayDuaThamGia;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class UpdateThamGiaFrm extends JFrame implements ActionListener{
    private final int WIDTH = 500;
    private final int HEIGHT = 500;
    private JLabel labelChang, labelDoiDua;
    private JButton btnSave1, btnSave2;
    private JTable  thamGiaTable;
    private DefaultTableModel dataModel;
    private List<TayDuaDangKi> listThamGia;
    private String[] columns = {"Ten", "Tuoi", "Quoc Tich"};
    private JScrollPane pane;


    private TayDuaDangKi tayDuaDangKi;

    public UpdateThamGiaFrm(TayDuaDangKi tayDuaDangKi){
        this.tayDuaDangKi = tayDuaDangKi;
        this.setSize(WIDTH + 100, HEIGHT);
        this.setLayout(null);

        initWidgets();

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        TayDuaDangKiDAO dangKiDAO = new TayDuaDangKiDAO();
        listThamGia = dangKiDAO.findTayDuaDangKi(tayDuaDangKi.getTenChangDua(), tayDuaDangKi.getTenDoiDua());

        final String[][] data = new String[listThamGia.size()][6];
        for(int i =0; i<listThamGia.size(); i++){
          final TayDuaDangKi  tg = listThamGia.get(i);
          data[i][0] = "" + tg.getTenChangDua();
          data[i][1] = "" + tg.getTenDoiDua();
          data[i][2] = "" + tg.getTenTayDua();
          data[i][3] = "" + tg.getTuoi();
          data[i][4] = "" + tg.getQuocTich();
          data[i][5] = "" + tg.getIdDangKi();
        }

        dataModel = new DefaultTableModel(null, columns);
        thamGiaTable = new JTable();
        thamGiaTable.setModel(dataModel);
        thamGiaTable.setBounds(10, 100, 400, 100);

        pane = new JScrollPane();
        pane.setViewportView(thamGiaTable);
        pane.setBounds(10, 100, 400, 100);
        thamGiaTable.getTableHeader().setBackground(Color.WHITE);
        this.add(pane);

        final String dataOut[][] = new String[listThamGia.size()][3];
        for (int i = 0; i < listThamGia.size(); i++) {
            //"Ten Tay Dua", "Tuoi", "Quoc tich", "Chang Dua", "Doi dua"
            final TayDuaDangKi dk = listThamGia.get(i);
            dataOut[i][0] = "" + dk.getTenTayDua();
            dataOut[i][1] = "" + dk.getTuoi();
            dataOut[i][2] = "" + dk.getQuocTich();
        }
        dataModel.setDataVector(dataOut, columns);
        dataModel.fireTableDataChanged();

        btnSave1 = new JButton("Xac nhan");
        btnSave1.setBounds(410,120,100,15);
        btnSave1.addActionListener(this);
        this.add(btnSave1);

        btnSave2 = new JButton("Xac nhan");
        btnSave2.setBounds(410,138, 100, 15);
        btnSave2.addActionListener(this);
        this.add(btnSave2);

    }
        void initWidgets(){
        labelChang = new JLabel("Ten chang dua: " + tayDuaDangKi.getTenChangDua());
        labelDoiDua = new JLabel("Ten doi dua: " + tayDuaDangKi.getTenDoiDua());
        labelName1 = new JLabel();
        labelChang.setBounds(100, 10, 200, 40);
        labelDoiDua.setBounds(100, 50, 200, 40);
        this.add(labelChang);
        this.add(labelDoiDua);
        //labelName1 = new JLabel()
        }
    @Override
    public void actionPerformed(ActionEvent e) {
        TayDuaDangKiDAO dkDAO = new TayDuaDangKiDAO();
        if ((e.getSource() instanceof JButton) && (e.getSource().equals(btnSave1))) {
            TayDuaThamGia tg1 = new TayDuaThamGia();
            final TayDuaDangKi dk1 = listThamGia.get(0);
            tg1.setTenChangDua(dk1.getTenChangDua());
            tg1.setTenDoiDua(dk1.getTenDoiDua());
            tg1.setTenTayDua(dk1.getTenTayDua());
            tg1.setIdDangKi(dk1.getIdDangKi());
            tg1.setTuoi(dk1.getTuoi());
            tg1.setQuocTich(dk1.getQuocTich());
            //INSERT INTO tblTayDuaThamGia(tenChangDua,tenDoiDua,tenTayDua,idDangKi,tuoi,quocTich)
            ThamGiaDao tgDAO = new ThamGiaDao();
            if(tgDAO.saveThamGia(tg1)){
                dkDAO.deleteARecord(dk1.getIdDangKi());
                JOptionPane.showMessageDialog(this, "Dang ki cho tay dua thu nhat thanh cong");
                btnSave1.getDisabledIcon();
            }
        }
        if ((e.getSource() instanceof JButton) && (e.getSource().equals(btnSave2))) {
            TayDuaThamGia tg2 = new TayDuaThamGia();
            final TayDuaDangKi dk2 = listThamGia.get(1);
            tg2.setTenChangDua(dk2.getTenChangDua());
            tg2.setTenDoiDua(dk2.getTenDoiDua());
            tg2.setTenTayDua(dk2.getTenTayDua());
            tg2.setIdDangKi(dk2.getIdDangKi());
            tg2.setTuoi(dk2.getTuoi());
            tg2.setQuocTich(dk2.getQuocTich());
            //INSERT INTO tblTayDuaThamGia(tenChangDua,tenDoiDua,tenTayDua,idDangKi,tuoi,quocTich)
            ThamGiaDao tgDAO = new ThamGiaDao();
            if (tgDAO.saveThamGia(tg2)) {
                JOptionPane.showMessageDialog(this, "Dang ki cho tay dua thu hai thanh cong");
                dkDAO.deleteARecord(dk2.getIdDangKi());
            }
        }
    }

}
